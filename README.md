This is a test to understand the CORS `Access-Control-Allow-Origin` response
header.

The question is

> If you have a preflight request that includes all the CORS headers, do you
> still require any CORS headers on the response for the *actual* request?

The answer is *yes*! The preflight response checks:

1. can this origin make a request at all?
1. can the specified method be used?
1. can the specified headers be sent?

Assuming that all passes, then you make the *actual* request. The response of
this request must also include CORS headers to allow the response to be read by
the JS on the page (can is read the body and/or headers).

# How to run

1. clone the repo
1. install deps
    ```bash
    pnpm install
    # or you can use `yarn`
    ```
1. start the server
    ```bash
    pnpm start
    # OR, to watch for code changes
    pnpm start:watch
    # you can also override the default port
    PORT=3003 pnpm start
    ```
1. open the UI using the link that is printed in the console
