const Koa = require('koa')
const router = require('koa-router')

const app = new Koa()
const routes = router()
const port = process.env.PORT || 3000

const customHeader = 'x-cors-on-resp'
const apiHost = '127.0.0.1'

routes.get('/', ctx => {
  ctx.body = `<body style="font-size: 2em;">
      <p>Welcome, make sure you're viewing this page on a domain that is
      <strong>NOT</strong> ${apiHost}. So use <a
      href="http://localhost:${port}">http://localhost:${port}</a> or anything
      else. We do this because we have to trigger a *cross origin* request.</p>
      <h3>How to use</h3>
      <ol>
        <li>open your browser devtools to follow the network requests.</li>
        <li>Use the button to trigger fetch requests.</li>
        <li>Make one <em>without</em> the checkbox checked, and you'll see a
        CORS error.</li>
        <li>Make another <em>with</em> the checkbox checked and everything will
        work.</li>

      </ol>
      <p>
        <label>
          <input type="checkbox" id="the-cb" />
          Set "Access-Control-Allow-Origin" on *POST* reponse
        </label>
      </p>
      <p>
        <button id="the-button">Make API call</button>
      </p>
      <script>
        document.getElementById('the-button').onclick = async function () {
          try {
            const isCorsOnResp = document.getElementById('the-cb').checked
            const resp = await fetch('http://${apiHost}:${port}/api', {
              method: 'POST',
              mode: 'cors',
              headers: {
                '${customHeader}': isCorsOnResp
              }
            })
            console.log('success', await resp.text())
          } catch (e) {
            alert('fail town')
            console.error('Failed to make call to our API', e)
          }
        }
      </script>
    </body>`
})

routes.options('/api', ctx => {
  console.log('OPTIONS hit')
  ctx.set('Access-Control-Allow-Origin', '*')
  ctx.set('Access-Control-Allow-Methods', 'GET,POST')
  ctx.set('Access-Control-Allow-Headers', customHeader)
  ctx.set('Access-Control-Max-Age', 'null')
  ctx.status = 204
})

routes.post('/api', ctx => {
  console.log('POST hit')
  const isCorsHeader = ctx.headers[customHeader] === 'true'
  if (isCorsHeader) {
    console.log('Including CORS headers on POST response')
    ctx.set('Access-Control-Allow-Origin', '*')
  } else {
    console.log('NOT including CORS headers on POST response')
    // technically this header would not be included in the response, but doing
    // this makes it more obvious that it's this header that controls the error
    ctx.set('Access-Control-Allow-Origin', 'cors-error-for-you-hahaha')
  }
  ctx.body = {
    success: true,
    message: 'yay'
  }
})

app.use(routes.routes())
console.log(`Open the UI at http://localhost:${port}`)
app.listen(port)
